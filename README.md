# CCA - Cloud Computing and Applications - Final Project

## Class Information
Instructor: Dr. Phu Phung  
Department: Department of Computer Science  
School: University of Dayton  

## Project Information
This is a team repository for the final project for the CCA - Clound Computing and Applications class

## Team Members
1. Brandon Collins - collinsb9@udayton.edu
2. Will Keith - willkeithstudent@icloud.com 

## Micro Service 1 Description
* Functional requirements
    * Input: one input field
    * Computation: perform a simple algorithm(s) to generate two output fields based on the input
    * Output: a string with the two generated fields
* Example: Weight lbs to kg (Kilograms) and st (Stone)
    * Input: Weight
    * Computation
        * Get the Stone = input_weight%14
        * Fix Stone to two Decimal point
        * Get the kg = intput_weight*0.45259237
        * Fix kg to two Decimal point
        * Compose Response
        * Send Response
    * Output: Converted Weight, e.g., “ 5 Lbs is equal to 0.357 st (Stone) or 2.267 kg (Kilograms)”