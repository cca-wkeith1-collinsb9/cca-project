const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
app.listen(port, () => 
    console.log(`HTTP Server with Express.js is listening on port:${port}`))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/index.html');
})

app.get('/WeightConversion', function (req, res) { 
    //Sprint1 Microservice 1
    //var data = "Edit in INdex.js GET"
    var data = req.query.data
    var St = data % 14
    var Kg = data * 0.45359237
    var convSt = St.toFixed(2);
    var convKg = Kg.toFixed(2);

    var response = `${data} Lbs is equal to ${convSt} st (Stone) or ${convKg} kg (Kilograms).`
    res.send(response)
})


app.get('/randomNumber/:num1/:num2', function (req, res) { 
    //Sprint1 Microservice 1
    //var data = "Edit in INdex.js GET"
    
    let num1 = req.params.num1;
    let num2 = req.params.num2;
    
    var int1 = parseInt(num1, 10);
    var int2 = parseInt(num2, 10);
    
    console.log(`Number 1 ${int1} and Number 2 ${int2}`)
    
    var result = '';
    
    do {
       result = randomNumber(int1, int2);
    }
    while (result < int1 && result > int2);

    var response = `${result} is a random number between ${num1} and ${num2}.`
    res.send(response)
})


function randomNumber(min, max) {  
  	//return Math.floor(Math.random() * max) + min; 
    return Math.floor(Math.random() * max); 
} 


// app.post('/echo.php', function (req, res) {
//     var data = req.body['data']
//     //var data = "Edit in INdex.js POST"
//     res.send(data)
// })